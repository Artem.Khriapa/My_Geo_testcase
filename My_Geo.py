from tools import ArgsParser, DefaultApiReq, DefaultSerializer


def main():
    try:
        cities = ArgsParser().cities
        res = DefaultApiReq(*cities).result
    except Exception as e:
        res = e

    serialized = DefaultSerializer().output_prepare(res)

    print(serialized)


if __name__ == "__main__":
    main()

