from sys import argv
from requests import get
import urllib


class CustomException(Exception):
    pass


class DefaultSerializer:
    """Description here"""

    fields = {'country', 'currency'}
    main_output_tpl = """\n{key}\n----------\n{data}----------\n"""


    def __init__(self, filename='fieds.txt'):

        if filename:
            ext_fields = self._get_external_fields(filename)
            if ext_fields:
                self.fields.update(ext_fields)

    def output_prepare(self, obj):

        if isinstance(obj, Exception):
            return self._formatter(self.exception_serializer(obj))
        return self._formatter(self.data_serializer(obj))

    @staticmethod
    def exception_serializer(obj):
        if isinstance(obj, CustomException):
            return {'Error': obj.args[0]}
        return {'Error': f'System Error. Please contact support. {obj.args[0]}'}

    def _formatter(self, data):

        res = ''
        for key, val in data.items():
            msg = ''
            if isinstance(val, str):
                msg += val + '\n'

            elif isinstance(val, dict):
                for k, v in val.items():
                    msg += f'{str(k).capitalize()}: {v}\n'

            res += self.main_output_tpl.format(key=key, data=msg)

        return res

    def data_serializer(self, obj):

        if not isinstance(obj, dict):
            return {'Error': f'System Error. Please contact support. Serialization error'}

        result = {}
        for city, citydata in obj.items():

            if not citydata:
                data = 'Invalid City Name'
            else:
                data = self._process_fields(citydata)

            result.update({city.capitalize(): data})

        return result

    def _process_fields(self, citydata):
        result = {}
        for field in self.fields:
            found_data = self._get_recursive(field, citydata)
            if found_data:
                result.update({field: found_data})

            # little hardcode for interaction with back4app api
            if 'country' in result and isinstance(result['country'], dict):
                res = self._get_recursive('code', result['country'])
                if res:
                    result[field] = res

        return result

    def _get_recursive(self, needed_key, data):
        for key in data:
            if key.lower() == needed_key.lower():
                return data[key]

        for val in data.values():
            if isinstance(val, dict):
                res = self._get_recursive(needed_key, val)
                if res:
                    return res

    @staticmethod
    def _get_external_fields(filename):

        try:
            with open('fields.txt', 'r') as file:
                return [field.rstrip().lstrip() for field in file]
        except:
            pass


class DefaultApiReq:
    """Description here"""

    _api_url = 'https://parseapi.back4app.com/classes/Continentscountriescities_City?limit=10&include=country&keys=name,country,country.code,country.capital,country.phone,country.native,country.currency,country.shape,population,location,cityId,adminCode&where={}'
    _apikey = 'eeaWNFLsgyXC2xNumaFTsMW4VJ2cX0VohWPv3Rcj'
    _apitoken = 'NZioUvFxk4HRcfXG6WZuBWYu08geIY12DLLPX7JX'
    _where_str = """{"name": {"$regex": "regexp"}}"""
    _cities = set()
    result = dict()

    def __init__(self, *cities):
        self._cities = set(cities)
        self.result = self.do_request()

    def do_request(self, *args):

        cities = args or self._cities
        tmp = dict()

        for city in cities:
            url = self._prepare_url(city)
            params = self._prepare_params()
            res = self._make_request(url, params)
            tmp |= {city: res['results'][0] if res['results'] else {}}

        return tmp

    @staticmethod
    def _normalize_to_regexp(city):

        raw = city.split()
        city = ' '.join([city.lower().capitalize() for city in raw])

        return f'^{city}$'

    def _prepare_url(self, city):

        city = self._normalize_to_regexp(city)

        data = self._where_str.replace('regexp', city)

        where = urllib.parse.quote_plus(data)

        return self._api_url.format(where)

    def _prepare_params(self):

        headers = {
            'X-Parse-Application-Id': self._apikey,
            'X-Parse-REST-API-Key': self._apitoken
        }

        return {'headers': headers}

    @staticmethod
    def _make_request(url, params):
        try:
            return get(url, **params).json()
        except Exception as e:
            raise CustomException('Invalid response')


class ArgsParser:
    """Description here"""

    cities = list()

    def __init__(self):

        self._raw_args = argv
        self.cities = self.get_city_names()

    def get_city_names(self):

        if len(self._raw_args) == 1:
            raise CustomException('Empty args')

        _args = self._raw_args[1:]

        if _args[0].lower() == '-f':
            try:
                return self._readfile(_args[1])

            except IndexError as e:
                msg = 'Empty filename'
            except FileNotFoundError as e:
                msg = 'Invalid filename'
            except Exception as e:
                msg = f'Application error {e.args}'

            if msg:
                raise CustomException(msg)

        else:
            return [city.rstrip().lstrip() for city in ' '.join(_args).split(',')]

    @staticmethod
    def _readfile(filename):

        with open(filename, 'r') as file:
            # normalize data here ?
            return [city.rstrip().lstrip() for city in file]

